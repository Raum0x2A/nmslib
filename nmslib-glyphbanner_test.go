package nmslib

import (
	"fmt"
	"testing"
)

func TestCreateBanner(t *testing.T) {
	fmt.Printf("\nTesting CreateBanner: ")

	rp := "21F2F8EDB94D"
	//rp := RndPortal()
	fileout := "./test"

	err := CreateBanner(rp, fileout, 0)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("Portal address `%s` rendered as `%s`\n", rp, fileout)
}
