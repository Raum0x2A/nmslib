package nmslib

import (
	"fmt"
	"testing"
)

func TestTranslate(t *testing.T) {
	kor2eng := Translate{Lang: "korvax"}
	if kor2eng.ToEng("Alinichel") != "Emergency" {
		t.Errorf("\nTesting Translate{Lang: \"korvax\"}.ToEng(\"Alinichel\"): got %q, want: %q.\n", kor2eng.ToEng("Alinichel"), "Emergency")
	} else {
		fmt.Printf("\nTesting Translate{Lang: \"korvax\"}.ToEng(\"Alinichel\"): got %q, want: %q.\n", kor2eng.ToEng("Alinichel"), "Emergency")
	}

	vyk2eng := Translate{"vykeen"}
	if vyk2eng.ToEng("Aqo") != "Foes" {
		t.Errorf("Testing Translate{Lang: \"vykeen\"}.ToEng(\"Aqo\"): got %q, want: %q.\n", vyk2eng.ToEng("Aqo"), "Foes")
	} else {
		fmt.Printf("Testing Translate{Lang: \"vykeen\"}.ToEng(\"Aqo\"): got %q, want: %q.\n", vyk2eng.ToEng("Aqo"), "Foes")
	}

	engWord := Translate{"english"}
	if engWord.ToKorvax("learn") != "achi" {
		t.Errorf("Testing Translate{Lang: \"english\"}.ToKor(\"learn\"): got %q, want: %q.\n", engWord.ToKorvax("learn"), "achi")
	} else {
		fmt.Printf("Testing Translate{Lang: \"english\"}.ToKor(\"learn\"): got %q, want: %q.\n", engWord.ToKorvax("learn"), "achi")
	}

	fmt.Println("Blind Test: " + Translate{"gek"}.ToAtlas("baturk")) // word does not exist in  Atlas lang
	fmt.Println("Blind Test: " + Translate{"gek"}.ToAtlas("Tor"))    // word exists

	fmt.Println("Running CleanUp()")
	CleanUp()
	fmt.Printf("\nTesting complete.\nStatus: ")
}
