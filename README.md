# NMSlib

[![](https://img.shields.io/badge/License%20-MIT-green)](COPYING)
[![GoDoc](https://godoc.org/gitlab.com/Raum0x2A/nmslib?status.svg)](https://godoc.org/gitlab.com/Raum0x2A/nmslib)
[![](https://img.shields.io/gitlab/pipeline/Raum0x2A/nmslib/master?label=Master&logo=go)](https://gitlab.com/Raum0x2A/nmslib)
[![](https://img.shields.io/gitlab/pipeline/Raum0x2A/nmslib/develop?label=Develop&logo=go)](https://gitlab.com/Raum0x2A/nmslib/-/tree/develop)
[![](https://img.shields.io/badge/Release-v1.0.1-blue?style=flat&logo=go)](https://gitlab.com/Raum0x2A/nmslib/-/releases/v1.0.1)

NMSlib is a GoLang package for dealing with converting Galactic coordinates to Portal codes and vice versa.

NMSlib also translates in game languages (Korvax, Gek Vy'Keen and Atlas) to english, and create portal banners.

## Installation

To install this module use `go get`


```golang
go get -u gitlab.com/Raum0x2A/nmslib
```

## Usage

### Example:

```golang
package main

import (
    "fmt"

    "gitlab.com/Raum0x2A/nmslib"
)

func main() {
    portalcode := "21F2F8EDB94D"
    galacticCoords, err := nmslib.P2gc(portalcode)
    if err != nil {
        panic(err)
    }
    fmt.Printf("Input:\t%s\nOutput:\t%s\n", portalcode, galacticCoords)
	
    g2p, err := nmslib.Gc2p("014C:0077:06DA:01F2", 2)
    if err != nil {
        panic(err)
    }
    fmt.Printf("Input:\t014C:0077:06DA:01F2\nOutput:\t%s\n", g2p)

    _ := nmslib.CreateBanner(portalcode, "./NMH-BlueDream.png", 0)
    _ := nmslib.CreateBanner(nmslib.RndPortal(), "./Random.png", 0)
	
    fmt.Println(nmslib.Translate{Lang: "korvax"}.ToEng("KIHTYOMOLES"))
    korvax := nmslib.Translate{"english"}
    fmt.Println(korvax.ToKorvax("Contraband"))
}


```

## Creating a Glyph banner

This function takes 3 parameters and reurns an error

* portalcode: 12 char hex string of portal location
* savename: Save location and name
* layout: 
  * 00 or 0 is horizontal in light mode
  * 01 or 1 is horizontal in dark mode
  * 11 is vertical in dark mode
  * 10 is vertical in light mode


**Example:**

Horizontal Layout for the NMH Capital planet [Blue Dream (aka Omins XI)](https://nomanssky.fandom.com/wiki/Omins_XI) in the [Notric-Lis XIII](https://nomanssky.fandom.com/wiki/Notric-Lis_XIII) system in the [Lahanhar Conflux](https://nomanssky.fandom.com/wiki/Lahanhar_Conflux) region

```golang
err := nmslib.CreateBanner("21F2F8EDB94D", "NMH-BlueDream.png", 0)
if err !=nil {
    panic(err)
}
```

![](NMH-BlueDream.png)

Vertical layout for a random address

```golang
err := nmslib.CreateBanner(nmslib.RndPortal(), "ranportal.png", 11)
if err != nil {
    panic(err)
}
```

![](ranportal_scaled.png)

_Note: Image shown in readme.md's vertical example have been scaled down in gimp for easier viewing on gitlab._

_Original ranportal.png: 256x3072px_


### Converting Portal code to Galactic address

This function only takes 1 parameter and returns a string and error

* portalcode: 12 char hex string of portal glyphs

**Example:**

```golang
ga, _ := nmslib.P2gc("21F2F8EDB94D")
fmt.Println(ga)
```

Output:

``014C:0077:06DA:01F2``

### Converting Galactic coordinates to Portal codes

This function only takes 2 parameters and returns a string and error

* galacticaddress: 16 char 4 block hex address
* Planet ID [1-6]

**Example:**

```golang
pc, _ := nmslib.Gc2p("014C:0077:06DA:01F2", 2)
fmt.Println(pc)
```

Output:

``21F2F8EDB94D``

## Translate Words

Supported languages

- Korvax
- Gek
- Vy'keen
- Atlas

### Translate Atlas to English

This function takes only 1 parameter and returns a string

* Word: Any known Atlas word (ex. Paka)

_note: Translating to english will **always** result in the lowercase equivilent word_

**Example:**

```golang
fmt.Println(nmslib.Translate{Lang: "atlas"}.ToEng("Paka"))
```

Output:

``awake``

### Translate English to Korvax

This function takes only 1 parameter and returns a string

* engword: English word to attempt conversion

**Example:**

```golang
korvax := nmslib.Translate{"english"}
fmt.Println(korvax.ToKorvax("Contraband"))
```

Output:

``Zelidovoso``


### Language commands

- ``Translate{Lang: ``string``}.ToEng(``string``)``
- ``Translate{Lang: ``string``}.ToKorvax(``string``)``
- ``Translate{Lang: ``string``}.ToGek(``string``)``
- ``Translate{Lang: ``string``}.ToVykeen(``string``)``
- ``Translate{Lang: ``string``}.ToAtlat(``string``)``

#### Possible Lang string options

- ``"english"``
- ``"korvax"``
- ``"gek"``
- ``"vykeen"``
- ``"atlas"``



## Random Portal address

Generate random portal codes
- Atlas Station
- Black Hole
- Random address

All random address created will default to the planet index 1 to help ensure there the address is valid

**Example:**

```golang
fmt.Println(nmslib.RndPortal())
fmt.Println(nmslib.RndPortal())
fmt.Println(nmslib.RndAtlas())
fmt.Println(nmslib.RndBH())
```

**Output examples:**

``10EFABDEA373``

``11EAB355FC8E``

``107AC0E190D1``

``10796D57AE15``

<hr>

## A Proud member of NMH Hub 

### [![NMH Official Page](https://static.wikia.nocookie.net/nomanssky_gamepedia/images/5/5a/NMH_Banner_Hub.png/revision/latest/scale-to-width-down/85?cb=20201210044657)](https://nomanssky.fandom.com/wiki/No_Man%27s_High_Hub) [![Discord Server](https://img.shields.io/badge/Discord-Invite-blue?&logo=discord&logoColor=white)](https://discord.gg/mhRxf29hns)
