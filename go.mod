module gitlab.com/Raum0x2A/nmslib

go 1.16

require (
	github.com/fogleman/gg v1.3.1-0.20210131172831-af4cd580789b
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	golang.org/x/image v0.0.0-20210504121937-7319ad40d33e // indirect
)
