package nmslib

import (
	"fmt"
	"testing"
)

func TestPortal2Galactic(t *testing.T) {
	want, err := P2gc("21F2F8EDB94D")
	if err != nil {
		fmt.Println(err)
	}
	if want != "014C:0077:06DA:01F2" {
		t.Errorf("Testing P2gc(\"21F2F8EDB94D\"): got %q, want: 014C:0077:06DA:01F2.\n", want)
	} else {
		fmt.Printf("Testing P2gc(\"21F2F8EDB94D\"): got: %q, want: 014C:0077:06DA:01F2.\n", want)
	}
}

func TestGalactic2portal(t *testing.T) {
	want, err := Gc2p("014C:0077:06DA:01F2", 2)
	if err != nil {
		fmt.Println(err)
	}
	if want != "21F2F8EDB94D" {
		t.Errorf("Testing Gc2p(\"014C:0077:06DA:01F2\"): got %q, want: 21F2F8EDB94D.\n", want)
	} else {
		fmt.Printf("Testing Gc2p(\"014C:0077:06DA:01F2\"): got: %q, want: 21F2F8EDB94D.\n", want)
	}
}
