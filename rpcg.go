/*
	[P][SSS][YY][ZZZ][XXX] – (P = Planet Index / S = Star System Index / Y = Height / Z = Width / X = Length)

	`Solar System Index
		A Solar System Index is assigned to each star system in a region. It always begins at SolarSystemIndex=001
		and counts up by one for every star system. The number of star systems in a region is variable so the
		maximum value of the Solar System Index is also variable, though the two correspond directly. To date there
		is no discovered value that is higher than SolarSystemIndex=243 (Mamundi-Kate in the Baadossm Anomaly of
		Euclid galaxy), meaning that 579 is the maximum number of star systems yet discovered in a region. Based
		on the evidence that every region has a SolarSystemIndex=079 and SolarSystemIndex=07A (with the former
		always having a Black Hole and the latter always having an Atlas Station), it is known that every region has
		at least 122 star systems. SolarSystemIndex=000 always leads to the region's first system, just like
		PlanetIndex=0 always leads to the first portal of a system due to the error proximity mechanic.`
	Source https://nomanssky.fandom.com/wiki/Portal_address#Solar_System_Index

*/

package nmslib

import (
	"fmt"
	"math/rand"
	"time"
)

/*
RndPortal - Random Portalcode generator
Requires no input required and returns a string
*/

func RndPortal() (final string) {
	rand.Seed(time.Now().UnixNano())
	min := 0x1
	ymax := 0xFF
	xzmax := 0xFFF
	ssimax := 0x242 // set this low to lower chances of an invalid address

	x := rand.Intn(xzmax-min+1) + min
	z := rand.Intn(xzmax-min+1) + min
	y := rand.Intn(ymax-min+1) + min
	ssi := rand.Intn(ssimax-min+1) + min

	final = fmt.Sprintf("%00X%03X%02X%03X%03X", 1, ssi, y, z, x)

	return
}

/*RndAtlas genorates a semi-random atlas code
Requires no input and returns a string

This is not a true random atlas genorator. Insted this will create a Planet id of 1, Solar System Index of 0x07A and random Y, Z, X
SSI 0x07A always has an Atlas Interface.

REF: https://nomanssky.fandom.com/wiki/Portal_address#Solar_System_Index https://nomanssky.fandom.com/wiki/Star_system#Classification
*/
func RndAtlas() (final string) {
	rand.Seed(time.Now().UnixNano())
	min := 0x1
	ymax := 0xFF
	xzmax := 0xFFF

	x := rand.Intn(xzmax-min+1) + min
	z := rand.Intn(xzmax-min+1) + min
	y := rand.Intn(ymax-min+1) + min

	final = fmt.Sprintf("%00X%03X%02X%03X%03X", 1, 0x07A, y, z, x)

	return
}

/*RndBH genorates a semi-random atlas code
Requires no input required and returns a string

This is not a true random atlas genorator. Insted this will create a Planet id of 1, Solar System Index of 0x079 and random Y, Z, X
SSI 0x079 always has a black hole.

REF: https://nomanssky.fandom.com/wiki/Portal_address#Solar_System_Index https://nomanssky.fandom.com/wiki/Star_system#Classification
*/
func RndBH() (final string) {
	rand.Seed(time.Now().UnixNano())
	min := 0x1
	ymax := 0xFF
	xzmax := 0xFFF

	x := rand.Intn(xzmax-min+1) + min
	z := rand.Intn(xzmax-min+1) + min
	y := rand.Intn(ymax-min+1) + min

	final = fmt.Sprintf("%00X%03X%02X%03X%03X", 1, 0x079, y, z, x)

	return
}
