package nmslib

import (
	"encoding/csv"
	"os"
	"strings"
)

/*Translate Game languages and english
Translate.Lang sets source language for translations
options are:
  korvax
  gek
  vykeen
  atlas
  english
*/
type Translate struct {
	Lang string
}

/*Translate source language to english
Translate any NMS language (ie. Korvax) to english
*/
func (t Translate) ToEng(word string) (translation string) {
	//return translate2english(word, langFile(t.Lang))
	return Translator(word, t.Lang, "english")
}

/*Translate source language to Korvax
Translate any NMS language or english (ie. Gek) to Korvax
*/
func (t Translate) ToKorvax(word string) (translation string) {
	return Translator(word, t.Lang, "korvax")
}

/*Translate source language to Vy'Keen
Translate any NMS language or english (ie. Atlas) to Vy'Keen
*/
func (t Translate) ToVykeen(word string) (translation string) {
	return Translator(word, t.Lang, "vykeen")
}

/*Translate source language to Gek
Translate any NMS language or english (ie. Vy'Keen) to Gek
*/
func (t Translate) ToGek(word string) (translation string) {
	return Translator(word, t.Lang, "Gek")
}

/*Translate source language to Atlas
Translate any NMS language or english (ie. Korvax) to Atlas
*/
func (t Translate) ToAtlas(word string) (translation string) {
	return Translator(word, t.Lang, "atlas")
}

// read language files
func langFile(lang string) [][]string {
	filepath := NmsTemp + "/assets/lang/" + lang + "-lang.csv"
	csvFile, err := os.Open(filepath)
	if err != nil {
		panic(err)
	}
	defer csvFile.Close()

	lines, err := csv.NewReader(csvFile).ReadAll()
	if err != nil {
		panic(err)
	}
	return lines
}

/*Translate any in game language to another including english (ie. Korvax to Gek)
Translating from english to a game language will alway default to all lower case for now
*/
func Translator(word string, source string, target string) string {
	var sourcekey [4]string
	var targetkey [4]string
	var dmy string
	var rt string

	rt = "*\\Kzzzzzzt\\*"

	if source == "english" {
		dmy = target
	} else {
		dmy = source
	}
	for _, line := range langFile(dmy) {
		for _, trans := range line {
			if trans == word {
				sourcekey[0] = line[0]
				sourcekey[1] = line[1]
				sourcekey[2] = line[2]
				sourcekey[3] = line[3]
			}
		}
		if target != "english" {
			for _, line := range langFile(target) {
				if strings.EqualFold(line[0], sourcekey[0]) {
					targetkey[0] = line[0]
					targetkey[1] = line[1]
					targetkey[2] = line[2]
					targetkey[3] = line[3]
				}
			}
			if strings.EqualFold(word, sourcekey[0]) {
				if targetkey[0] != "" {
					rt = targetkey[0]
				}
			} else if strings.EqualFold(word, sourcekey[1]) {
				if targetkey[1] != "" {
					rt = targetkey[1]
				}
			} else if strings.EqualFold(word, sourcekey[2]) {
				if targetkey[2] != "" {
					rt = targetkey[2]
				}
			} else if strings.EqualFold(word, sourcekey[3]) {
				if targetkey[3] != "" {
					rt = targetkey[3]
				}
			}
			if source == "english" {
				rt = targetkey[1]
			}
		} else {
			if strings.EqualFold(word, sourcekey[0]) {
				rt = sourcekey[0]
			} else if strings.EqualFold(word, sourcekey[1]) {
				rt = strings.ToLower(sourcekey[0])
			} else if strings.EqualFold(word, sourcekey[2]) {
				rt = strings.Title(sourcekey[0])
			} else if strings.EqualFold(word, sourcekey[3]) {
				rt = strings.ToUpper(sourcekey[0])
			}
		}
	}
	return rt
}
