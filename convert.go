/*
	Xainesworld	Video: https://www.youtube.com/watch?v=xmZbkTahw4w
	Fandom Wiki: https://nomanssky.fandom.com/wiki/Portal_address
*/

package nmslib

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

/*
P2gc - Portal code to galactic coordinates
Requires 1 var and returns 1 var string and an error
var p string: Portal Glyph hex string 12 chars in len (ex. 006afa556c30)
Returns var string: Galactic address (ex. 042F:0079:0D55:006A)
*/
func P2gc(p string) (gc string, err error) {
	if len(p) == 12 { // Test if length of provided string is 12 chars long
		//split sting into X, Y, Z and SSI, and convert coords as an array of intergers
		// p[0:0] is not needed and will be ignored
		var coord [4]int64
		coord[1], err = strconv.ParseInt(p[4:6], 16, 16) // Y coordinate
		if err != nil {
			return "", errors.New("error parsing intergers from string: [4:6]")
		}
		coord[2], err = strconv.ParseInt(p[6:9], 16, 16) // Z cooridnate
		if err != nil {
			return "", errors.New("error parsing intergers from string: [6:9]")
		}
		coord[3], err = strconv.ParseInt(p[9:12], 16, 16) // X coordinate
		if err != nil {
			return "", errors.New("error parsing intergers from string: [9:12]")
		}
		coord[0], err = strconv.ParseInt(p[1:4], 16, 16) // SSI (Star System Identifier)
		if err != nil {
			return "", errors.New("error parsing intergers from string: [1:4]")
		}

		// apply shifts to Handle the shape/boundaries of the galaxy
		if coord[1] <= 128 { // Y
			coord[1] = coord[1] + 256 // Y = Y + 0x0100
		}
		if coord[2] <= 2048 { // Z
			coord[2] = coord[2] + 4096 // Z = Z + 0x1000
		}
		if coord[3] <= 2048 { // X
			coord[3] = coord[3] + 4096 // X = X + 0x1000
		}
		coord[1] = coord[1] - 129  // Y = Y - 0x0081
		coord[2] = coord[2] - 2049 // Z = Z - 0x0800
		coord[3] = coord[3] - 2049 // X = X - 0x0800

		//Format coords as a 4 part hex string (X:Y:Z:SSI) with a zero pad of 4 (ex. 042F:0079:0D55:006A)
		gc = fmt.Sprintf("%04X:%04X:%04X:%04X", coord[3], coord[1], coord[2], coord[0])

	} else { // if len(p) != 12 return an error
		return "", errors.New("a 12char HEX string is required. example: 006afa556c30")
	}
	return // return formated string
}

/*
Gc2p - Galactic coordinates to portal code
Requires 1 string and 1 int returns a string and error
var gc string: Galactic address (ex. 042F:0079:0D55:006A)
var p int: Planet ID [1-6]
Returns portalcode string: Portal Glyph hex string 12 chars in len (ex. 006afa556c30)
*/
func Gc2p(gc string, p int) (portalcode string, err error) {
	// split and store string
	// coords[0] == X; coords[1] == Y coords[2] == Z;
	// coords[3] == SSI coords[4] == P
	if len(gc) != 19 {
		return "", errors.New("galatic code is the wrong length")
	}
	if p > 6 {
		p = 1
	}
	nustring := fmt.Sprintf("%s:%d", gc, p)
	coords := strings.Split(nustring, ":")
	for n := 0; n < len(coords); n++ {
		portalcode = portalcode + coords[n]
	}
	//convert coords to an array of intergers
	var hexCoords [5]int64
	hexCoords[0], err = strconv.ParseInt(coords[4], 16, 16) // P
	if err != nil {
		return "", errors.New("error parsing intergers from string: [4]")
	}
	hexCoords[1], err = strconv.ParseInt(coords[3], 16, 16) // SSI
	if err != nil {
		return "", errors.New("error parsing intergers from string: [3]")
	}
	hexCoords[2], err = strconv.ParseInt(coords[1], 16, 16) // Y
	if err != nil {
		return "", errors.New("error parsing intergers from string: [1]")
	}
	hexCoords[3], err = strconv.ParseInt(coords[2], 16, 16) // Z
	if err != nil {
		return "", errors.New("error parsing intergers from string: [2]")
	}
	hexCoords[4], err = strconv.ParseInt(coords[0], 16, 16) // X
	if err != nil {
		return "", errors.New("error parsing intergers from string: [0]")
	}

	// Apply shifts to Handle the shape/boundaries of the galaxy
	/*Note:
	[P][SSS][YY][ZZZ][XXX] len == 12
	* SSS == SSI
	*/
	hexCoords[2] = hexCoords[2] + 0x81  // Y ->> shift
	hexCoords[3] = hexCoords[3] + 0x801 // Z ->> shift
	hexCoords[4] = hexCoords[4] + 0x801 // X ->> shift
	if hexCoords[3] >= 0x1000 {
		hexCoords[3] = hexCoords[3] - 0x1000 // Z <<- shift
	}
	if hexCoords[2] >= 0x100 {
		hexCoords[2] = hexCoords[2] - 0x100 // Y <<- shift
	}

	// Assemble padded values as a string

	portalcode = fmt.Sprintf("%00X%03X", hexCoords[0], hexCoords[1])
	for n := 2; n < len(hexCoords); n++ {
		if n == 2 {
			portalcode = portalcode + fmt.Sprintf("%02X", hexCoords[n])
		} else {
			portalcode = portalcode + fmt.Sprintf("%03X", hexCoords[n])
		}
	}
	return // return formated string and error
}
