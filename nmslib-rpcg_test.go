package nmslib

import (
	"fmt"
	"testing"
)

func TestRndPortal(t *testing.T) {
	fmt.Printf("\nRandom Portal Code: ")
	fmt.Println(RndPortal())
}

func TestRndAtlas(t *testing.T) {
	fmt.Printf("semi-Random Atlas Code: ")
	fmt.Println(RndAtlas())
}

func TestRndBH(t *testing.T) {
	fmt.Printf("semi-Random black hole Code: ")
	fmt.Println(RndBH())
}
